package com.example.ocrexperiment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView_onDeviceResult;
    private TextView textView_cloudResult;

    private static final int REQ_CODE_CAMERA_PERMISSION = 1;
    private static final int REQ_CODE_IMAGE_CAPTURE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageview);
        textView_onDeviceResult = findViewById(R.id.text_ondevice_result);
        textView_cloudResult = findViewById(R.id.text_cloud_result);

        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQ_CODE_CAMERA_PERMISSION);
        }
    }

    public void doProcess(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            startActivityForResult(intent, REQ_CODE_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Camera is not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
            imageView.setVisibility(View.VISIBLE);

            recognizeTextOnDevice(imageBitmap);
            recognizeTextOnCloud(imageBitmap);
        }
    }

    private void recognizeTextOnDevice(Bitmap bitmap) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);

        FirebaseVisionTextRecognizer textRecognizer = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
        Task<FirebaseVisionText> task = textRecognizer.processImage(image);
        task.addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
            @Override
            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                Toast.makeText(MainActivity.this, "Text recognition on device success" + firebaseVisionText.getText(), Toast.LENGTH_LONG).show();
                String s = firebaseVisionText.getText();
                textView_onDeviceResult.setText(s);
                textView_onDeviceResult.setVisibility(View.VISIBLE);
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this, "Text recognition on device failed", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void recognizeTextOnCloud(Bitmap bitmap) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionTextRecognizer textRecognizer = FirebaseVision.getInstance().getCloudTextRecognizer();

        Task<FirebaseVisionText> task = textRecognizer.processImage(image);
        task.addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
            @Override
            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                Toast.makeText(MainActivity.this, "Text recognition on cloud success" + firebaseVisionText.getText(), Toast.LENGTH_LONG).show();
                String s = firebaseVisionText.getText();
                textView_cloudResult.setText(s);
                textView_cloudResult.setVisibility(View.VISIBLE);
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this, "Text recognition on cloud failed"+e.getMessage(), Toast.LENGTH_LONG).show();
                textView_cloudResult.setText("Text recognition on cloud failed"+e.getMessage());
                textView_cloudResult.setVisibility(View.VISIBLE);
                Log.d("OCR ERROR", e.getMessage());
            }
        });

    }
}